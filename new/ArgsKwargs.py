def myFun1(*argv):  
    for arg in argv:  
        print (arg) 

def myFun(**kwargs):  
    for key, value in kwargs.items(): 
        print ("%s == %s" %(key, value)) 
  
# Driver code 
print("args\n")
myFun1('Hello', 'Welcome', 'to', 'GeeksforGeeks')  

print("\nKwargs\n")
myFun(first ='Geeks', mid ='for', last='Geeks')


'''
args:

The special syntax *args in function definitions in python is 
used to pass a variable number of arguments to a function. 
It is used to pass a non-keyworded, variable-length argument list.

kwargs:
The special syntax **kwargs in function definitions in python is used 
to pass a keyworded, variable-length argument list. We use the name 
kwargs with the double star. The reason is because the double star allows 
us to pass through keyword arguments (and any number of them).
'''