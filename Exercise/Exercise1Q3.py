
#Using user defined function.
def titleCase(name):
    newname=""
    for i in name:
        temp=list(i)
        if ord(temp[0])>=97:
            # temp[0]=temp[0].capitalize()
            temp[0]=chr(ord(temp[0])-32)
            newname+="".join(temp)
            newname+=" "
    return newname
name =input("Enter your full name: ").split()
newName=titleCase(name)
print("using user defined functions:",newName)

#Using in built function
name=input("Enter your full name: ")
print("using built in function: ",name.title())
