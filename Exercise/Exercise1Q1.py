# import math
#1 to 10 factorial using for loop

print("Factorials from 1 to 10 using for loop")
for i in range(1,11):
    fact=1
    print(i,end=": ")
    while i!=0:
       fact=fact*i
       i-=1
    print(fact)
    
#Factorial using recursion

def factorial(i):
    if i==1:
        return i
    else:
        return i*factorial(i-1)
    

print("Factorials from 1 to 10 using Recursion")
for i in range(1,11):
    fact=1
    fact=factorial(i)
    print(fact)