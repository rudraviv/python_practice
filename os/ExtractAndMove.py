import os
import pathlib
import shutil
import zipfile as z

# Modify within double quotes only
zip_path=r"C:\Users\admin\Desktop\Rudra"

source_folder=r'C:\Users\admin\Desktop\Rudra'
move_extension=".py"
dest=r"C:\Users\admin\Desktop\Akshay"



def getFiles(source_folder,ext):
        file_path=[]
        for (root,dirs,files) in os.walk(source_folder, topdown=True): 
                # print (root) 
                # print (dirs )
                # print (files)
                for file in files:
                        if file.endswith(ext) and root.find("$")==-1:#find is use to eliminate recycle bin files
                                # print ((root+''+str(file))) # file with path.
                                file_path.append((root+'/'+str(file)))

        # print(file_path)
        return file_path

def moveFiles(source_folder,dest,mve):
        count=0
        file_path=getFiles(source_folder,mve)
        try:
                for src in file_path:
                        shutil.move(src, dest)
                        count+=1
                print("Files moved:",count)
        except:
                print("Something went wrong")

def extractZip(path):
    filePath=getFiles(path,".zip")
    count=0
    for i in filePath:
        try:
                zip_ref = z.ZipFile(i) # create zipfile object
                zip_ref.extractall(path) # extract file to dir
                count+=1
                zip_ref.close() # close file
        except:
                print("something went wrong")
    print("zipped files extracted:",count)


extractZip(zip_path)
moveFiles(source_folder,dest,move_extension)