#dictionary demo
#dictionaries are mutable

student={'name':'Rudra','age':25,'courses':['DAC','DBDA','ES']}

print(student)
print(student['courses'])#print courses only
#print(student['phone'])#will give key error
print(student.get('name'))#will print name
print(student.get('courses'))
print(student.get('phone'))#will return None instead of KeyError
print(student.get('phone','Not Found'))#will print Not 

#update Dictionary
student['name']='akshay'#here we updating in one key
print(student)
#to update multiple values we need to use update method(keys and values)
student.update({'name':'akshay','age':25,'courses':['DAC']})

print(len(student))
print(student.keys())
print(student.values())
print(student.items())
print(student)