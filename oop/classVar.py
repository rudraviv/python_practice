#demo for class variable
class Employee:
    raise_amt= 0.2
    def __init__(self,first,last,pay): #Constructor
        self.first=first
        self.last=last
        self.pay=pay
        self.email=first+'.'+last+'company.com'
    
    def fullname(self):
        return self.first+" "+self.last
    
    def apply_raise(self):
       # self.pay=int(self.pay*1.04)#hardcoded value
         self.pay=int(self.pay*Employee.raise_amt)
         return self.pay

emp1=Employee("Rudra","Viv",50000)
emp2=Employee("testprint(Employee.apply_raise())","test",60000)

print(Employee.raise_amt)
print(emp1.raise_amt)
print(emp2.raise_amt)

Employee.raise_amt=4
print("\n")
print(Employee.raise_amt)
print(emp1.raise_amt)
print(emp2.raise_amt)

emp1.raise_amt=0.1
print("\n")
print(Employee.raise_amt)
print(emp1.raise_amt)
print(emp2.raise_amt)


print(emp1.apply_raise())
print(emp2.apply_raise())


'''
class variables are variable that are
shared among all instances of class.

class variable are same for all instances.
'''
