#Also known as special method or magic methods.
#allows operator overloading.
#by using these methods we can change built in behavior.

class Employee:
    raise_amt=1.04

    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.email=first+'.'+last+'@email.com'
        self.pay=pay
    
    def fullname(self):
        return f"{self.first} {self.last}"
    
    def apply_raise(self):
        self.pay=int(self.pay*self.raise_amt)
  
    def __repr__(self): #ubambiguous representation of object and should be used for debugging,logging etc.
        return f"{self.first}{self.last}  {self.email}  {self.pay}"

    def __str__(self):#use for readability of object. specifically it is use to display object to end user.
        return f"{self.first}{self.last}  {self.email}"

    def __add__(self,other):
        return self.pay+other.pay 

emp1=Employee('rudra','viv',90000)
emp2=Employee('Test','ing',60000)
# print(emp1)#output:<__main__.Employee object at 0x00E4E7F0> when repr and str are commented.
print("default dunder: ",emp1)
print("with repr: ",emp1.__repr__())
print("with str: ",emp1.__str__())

#operator overloading
#suppose we want to add object and display sum of salary

print("total salary payable: ",emp1+emp2)
