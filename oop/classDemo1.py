class Employee:
    def __init__(self,first,last,pay): #Constructor
        self.first=first
        self.last=last
        self.pay=pay
        self.email=first+'.'+last+'company.com'
    
    def fullname(self):
        return self.first+" "+self.last

emp1=Employee("Rudra","Viv",50000)
emp2=Employee("test","test",60000)

print(emp1)#print about object
print(emp1)

print(emp1.email)

print(emp1.fullname())
print(emp2.fullname())


'''
1)__init__ called as constructor

2)each method of class automatically 
takes the instance as a first argument.(self)

3)to every method, object is passed as a argument internally.so
it is mendatory to write "self" as a catching variable inside function definition.
 
'''