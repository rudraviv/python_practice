#demo for class method 

class Employee:
    no_of_leaves = 8

    def __init__(self, aname, asalary, arole):
        self.name = aname
        self.salary = asalary
        self.role = arole

    def printdetails(self):
        return f"The Name is {self.name}. Salary is {self.salary} and role is {self.role}"

    @classmethod
    def change_leaves(cls, newleaves):
        cls.no_of_leaves = newleaves

    @classmethod
    def from_dash(cls, string):
        # params = string.split("-")
        # print(params)
        # return cls(params[0], params[1], params[2])
        return cls(*string.split("-"))


harry = Employee("Rudra", 255, "Instructor")
rohan = Employee("Rohan", 455, "Student")
akshay = Employee.from_dash("Akshay-480-Student")

print(akshay.no_of_leaves)
print(akshay.role)
'''
**********NOTES**********

1)@classmethod Decorator:

-The @classmethod decorator, is a builtin function decorator that is an expression 
 that gets evaluated after your function is defined. The result of that evaluation 
 shadows your function definition.

-A class method receives the class as implicit first argument, just like an instance 
 method receives the instance.

-A class method is a method which is bound to the class and not the object of the class.

-They have the access to the state of the class as it takes a class parameter that points
 to the class and not the object instance.

-It can modify a class state that would apply across all the instances of the class.
 For example it can modify a class variable that will be applicable to all the instances.

-Extra:-Methods which have to be created with the decorator @classmethod, 
        this methods share a characteristic with the static methods and that is that 
        they can be called without having an instance of the class. The difference relies 
        on the capability to access other methods and class attributes but no instance attributes.

-Mostly,class method is used for alternative constructor means changing functionality of constructor without 
 modifying actual constructor .
'''