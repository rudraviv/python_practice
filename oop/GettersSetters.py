class Employee:
    raise_amt=1.04

    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.email=first+'.'+last+'@email.com'
        self.pay=pay
    
    def fullname(self):
        return f"{self.first} {self.last}"
    
emp1=Employee("John","Rambo",5000)

#figure out below commented code first
# print(emp1.fullname())
# print(emp1.email)
# emp1.first="jim"
# print(emp1.fullname())
# print(emp1.email)
