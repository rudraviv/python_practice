#advance 3
class Employee:
    raise_amt=1.04

    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.email=first+'.'+last+'@email.com'
        self.pay=pay
    
    def fullname(self):
        return f"{self.first} {self.last}"
    
    def apply_raise(self):
        self.pay=int(self.pay*self.raise_amt)


class Developer(Employee):
    raise_amt=1.10
    def __init__(self,first,last,pay,prog_lang):
        super().__init__(first,last,pay)
        self.prog_lang=prog_lang

class Manager(Employee):
    
    def __init__(self,first,last,pay,employees=None):
        
        super().__init__(first,last,pay)
        
        if employees is None:
            self.employees=[]
        else:
            self.employees=employees
    
    def add_emp(self,emp):
        if emp not in self.employees:
            self.employees.append(emp)
    
    def remove_emp (self,emp):
        if emp in self.employees:
            self.employees.remove(emp)
    
    def print_emp(self):
        print("*****Employee List********")
        for emp in self.employees:
            print(f"{emp.fullname()}")

dev1=Developer("Rudra","Viv",50000,'Python')
emp1=Employee("abc","pat",60000)
dev2=Developer("xyz","Chau",40000,"Java")

mgr1=Manager("pqr","kshir",50000,[dev1,emp1])#supervising above both emp
mgr1.print_emp()

mgr1.add_emp(dev2)
mgr1.print_emp()

mgr1.remove_emp(emp1)
mgr1.print_emp()

