list1=[10,2,3.4,5.4,45]
list2=list1

print("list1: ",list1)
print("After assignment list2 to list1")
print("list1: ",list1)
print("list2",list2)

list1[1]="Hello"

print("After changing content of list1")
print("list1:",list1)
print("list2:",list2)


#List Copying

l3=list(list1)
print("After copying list1 to list3")
print("list3: ",l3)