#Replace Operation    List are mutable
list1=[10,20,40,15.5,1]
list1[2]=[3.5] #inserting a list at 2nd location
list1[1]=2.5
print(list1)

#Insert Operation
list1.insert(2,"Hello")
print(list1)

#remove operation....this will remove given element present in list
#If value not present in list then it will give value error.

list1.remove([3.5])
list1.remove("Hello")
print(list1)

#del operation.....this will remove value present at given index
del list1[1]
print(list1)
list2=['abc',2,3,4,5.7]
print(list2)
del list2       #Here we deleted list2 

#Sort Operation
print(sorted(list1))    #here only print sorted list.
list1.sort()     #here  modified original list in sorted form
print(list1)
#append Operation