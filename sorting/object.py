#Trick to sort objects


class Employee():
    def __init__(self,name,age,salary):
        self.name=name
        self.age=age
        self.salary=salary
    
    def __repr__(self):
        return f'({self.name},{self.age},{self.salary})'


e1=Employee('Rudra',24,80000)
e2=Employee('Lokesh',30,50000)
e3=Employee('Akshay',25,60000)

emp=[e1,e2,e3]

def e_sort(emp):
    return emp.age #here we are sorting on age basis

s_emp=sorted(emp,key=e_sort)#here e_sort return age.so sorting operates on age basis

print(s_emp)

'''
---Note----
1)in above program we can write lambda function in sorted funciton instead of writing separate function(e_sort)
2)we can import operator's attrgetter method. we assign age as a parameter to attrgetter and this attrgetter returns 
value to key. i.e key=attrgette('age')
'''