#sequence operations perform on tuple,string and list.

name="Python is vety interesting language."
l1=[10,20,3.9,40,50,6]
tuple1=(10,2.2,3,9)

#Length 
print(len(name))
print(len(l1))
print(len(tuple1))

#Select
print(name[2])
print(l1[1])
print(tuple1[3])

#Slice
print(name[0:6])
print(name[7:])
print(name[1::-1])

#count
print(name.count('i'))
print(l1.count(15))

#index
print(name.index("v"))