x=123 #global

def display():
    y=23
    print(f'y Inside funtion: {y}')
    print(f'Global inside function: {globals()["x"]}')
    x=44
    print(f'Reassigned x inside function:{x}')


print(f'Accessing x outside function: {x}\n\n')
#print(y)#cant access becoz y is local
print("\ncalling display\n")
#display()
z=display #Assigning a function to variable
z()
